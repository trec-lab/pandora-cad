# PANDORA CAD


Welcome to the PANDORA project! The PANDORA project is an open-source design of a full-sized humanoid robot that main structural elements are 3D printed.

## Getting Started with the CAD

The file that we have added is a step file of the lower and partial upper body assembly of PANDORA. The design has originally been completed in Siemens NX, but to allow more users to have access to the project we have saved it as a step file.

WHEN OPENING in the desired CAD software, we HIGHLY recommend that you have the step file in it’s own folder since the CAD software will unpack every part into the folder. 


## Printing of the Parts

Printing of all parts have been completed with a Creality Ender-5 and CR-10S Pro. We utilize CURA as our slicer and important settings is a 25% minimum cubic infill with a 4-wall thickness. We also recommend looking at parts before printing because in most areas support material can be blocked for an easier time of cleaning. The print material we use is ESUN PLA+. 


***
## Let us Know
We ask if you do utilize PANDORA that you give pictures, videos, and let us know because we really would like to see where our creation goes. 

## Support
Tell people where they can go to for help. If you have any issues fel free to message the TREC Lab.

## Versions
The current version given as of 2/21/2023 is 3.0 of PANDORA.



